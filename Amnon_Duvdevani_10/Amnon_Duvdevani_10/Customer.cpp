#include "Customer.h"

Customer::Customer(string name)
{
	_name = name;
	_items;
}

Customer::Customer()
{
}

Customer::~Customer()
{

}

double Customer::totalSum() const //returns the total sum for payment
{
	int sum = 0;
	set<Item>::iterator si;

	for(si = _items.begin(); si != _items.end(); si++)
	{
		Item itm = *si;
		sum = sum + itm.totalPrice();
	}
	return sum;
}

void Customer::addItem(Item other) //add item to the set
{
	_items.insert(other);
}

void Customer::deleteItem(Item other) //remove item from the set
{
	set<Item>::iterator si;

	si = _items.find(other);

	_items.erase(si);
}

bool Customer::item_exists(Item other)
{
	set<Item>::iterator si;

	si = _items.find(other);

	return si !=_items.end();
}

void Customer::inc_item(Item item)
{
	set<Item>::iterator si;

	si = _items.find(item);
	Item temp_item = *si;

	this->deleteItem(item);
	temp_item.inc_count();
	this->addItem(temp_item);

}

void Customer::print_items()
{
	int i = 1;
	set<Item>::iterator it;
	Item temp_item;
	
	cout << "\n" << _name << "'s cart:" << endl;

	for (it = _items.begin(); it != _items.end(); it++, i++)
	{
		temp_item = *it; // set temp item
		
		cout << temp_item.get_name() << "  amount: " << temp_item.get_count() << endl;
	}
}

void Customer::remove_item(Item item)
{
	set<Item>::iterator si;

	si = _items.find(item);
	Item temp_item = *si;

	this->deleteItem(item);
	if (temp_item.get_count() > 1)
	{
		temp_item.dec_count();
		this->addItem(temp_item);
	}
}

string Customer::get_name()
{
	return _name;
}