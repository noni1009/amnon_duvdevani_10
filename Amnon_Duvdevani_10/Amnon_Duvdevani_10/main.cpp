#include"Customer.h"
#include <iostream>
#include<map>
#include<vector>
#include"Item.h"

int printMenu();
bool user_exists(string name, map<string, Customer> users);
void buy_items(vector<Item> itemList, Customer* user);
int print_items(vector<Item> itemList);
void storeItems(vector<Item>* itemList);
void removeItem(vector<Item> itemList, Customer* user);

int main()
{
	int userChoide = -1;
	string userName;
	map<string, Customer> abcCustomers;	// map of customers(sorted by name)
	vector<Customer> priceCustomers;	
	vector<Item> itemList;				// vector of items in shop

	storeItems(&itemList);
	
	while (userChoide != 4)
	{
		system("cls");
		userChoide = printMenu();

		switch (userChoide) {
		case 1:						// sign in returns bool(if user already exists return true)
			cout << "\nEnter new user name: ";
			cin >> userName;

			if (!user_exists(userName, abcCustomers))
			{
				// create new user 
				Customer* cstmr = new Customer(userName);
				
				// add items to user
				buy_items(itemList, cstmr);
			
				// add user to "abcCustomers"
				abcCustomers.insert(pair<string, Customer>(userName, *cstmr));

			}
			else
			{
				cout << "User Error" << endl;
			}
			break;

		case 2:
			cout << "\nEnter existing user name: ";
			cin >> userName;
			cout << endl;

			if (user_exists(userName, abcCustomers))
			{
				int op = -1;

				while (op != 3)
				{
					abcCustomers.find(userName)->second.print_items();	// print user items
				
					cout << "1. Add items" << endl;
					cout << "2. Remove items" << endl;
					cout << "3. Back to menu" << endl;
					cout << "\n" << endl;
					cin >> op;
					while (op < 1 || op > 3)
						cin >> op;
					
					if (op == 3)
						break;
					if (op == 1)
					{
						buy_items(itemList, &abcCustomers.find(userName)->second);
					}
					if (op == 2)
					{
						removeItem(itemList, &abcCustomers.find(userName)->second);
					}
					
				}
			}
			else
			{
				cout << "User Error" << endl;
			}
			break;

		case 3:
			int maxPayment = 0;	
			map<string, Customer>::iterator customers_it;
			Customer payer = Customer(); // object to save the highest pying customer into

			for (customers_it = abcCustomers.begin(); customers_it != abcCustomers.end(); customers_it++)
			{
				pair<string, Customer> pair_customer = *customers_it; // temporary pair
				
				if (pair_customer.second.totalSum() > maxPayment)	// check if current customer has bigger paying than current maxPayment
				{
					maxPayment = pair_customer.second.totalSum();	// update maxPayment
					payer = pair_customer.second;					// put current customer with biggest payment inside object
				}
			}
			
			// print info about highest payer
			cout << "\n\tCustomer to pay the most:" << endl;
			cout << "Customer name: " << payer.get_name() << endl;
			cout << "cart:\n";
			payer.print_items();
			cout << "to pay: " << payer.totalSum() << endl;
			break;
		}
	}
	system("pause");
	return 0;
}

void removeItem(vector<Item> itemList, Customer* user)
{
	string itemToRemove;
	Item tempItemToRemove;

	cout << "Item to remove: ";
	cin >> itemToRemove;

	// makes first letter uppercase
	itemToRemove[0] = toupper(itemToRemove[0]);

	for (int i = 0; i < itemList.size(); i++)
	{
		if (itemToRemove == itemList[i].get_name()) {
			tempItemToRemove = itemList[i];
			break;
		}
	}

	user->remove_item(tempItemToRemove);
}

void buy_items(vector<Item> itemList, Customer* user)
{
	int choice = -1;

	while (choice != 0)
	{
		printf("\n");

		choice = print_items(itemList);
		
		if (choice == 0)
		{
			break;
		}

		// item that user choosed already in his set
		if (user->item_exists(itemList[choice - 1]))
		{
			// increase count of item
			user->inc_item(itemList[choice - 1]);
		}
		else
		{
			user->addItem(itemList[choice - 1]);
		}

	}
	
	// clean screen 
	system("cls");
}

int print_items(vector<Item> itemList)
{
	int choice = -1;

	cout << "The items you can buy are: (0 to exit)" << endl;
	for (int i = 0; i < itemList.size(); i++)
	{
		cout << i + 1 << "  " << itemList[i].get_name() << "    price: " << itemList[i].get_price() << endl;
	}
	while (choice < 0 || choice > 10)
		cin >> choice;

	return choice;
}

bool user_exists(string name, map<string, Customer> users)
{
	map<string, Customer>::iterator it;

	it = users.find(name);

	return it == users.end() ? false : true;
}

int printMenu()
{
	int choice = 0;

	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. to sign as customer and buy items" << endl;
	cout << "2. to uptade existing customer's items" << endl;
	cout << "3. to print the customer who pays the most" << endl;
	cout << "4. to exit" << endl;

	while (choice < 1 || choice > 4)
		cin >> choice;

	return choice;
}

void storeItems(vector<Item>* itemList)
{
	try {
		itemList->push_back(Item("Milk", "00001", 5.3));
		itemList->push_back(Item("Cookies", "00002", 12.6));
		itemList->push_back(Item("Bread", "00003", 8.9));
		itemList->push_back(Item("Chocolate", "00004", 7.0));
		itemList->push_back(Item("Cheese", "00005", 15.3));
		itemList->push_back(Item("Rice", "00006", 6.2));
		itemList->push_back(Item("Fish", "00008", 31.65));
		itemList->push_back(Item("Chicken", "00007", 25.99));
		itemList->push_back(Item("Cucumber", "00009", 1.21));
		itemList->push_back(Item("Tomato", "00010", 2.32));
	}
	catch (invalid_argument &e)
	{
		cout << "Invalid argument!" << endl;
	}
}