#include "Item.h"

using namespace std;

Item::Item(string prodName, string serialNum, double price)
{

	if (price > 0 && serialNum.length() == 5 && prodName.length() > 0)
	{
		_name = prodName;
		_serialNumber = serialNum;
		_unitPrice = price;
		_count = 1;
	}
	else
	{
		throw invalid_argument("wrong parameter inserted");
	}
}

Item::Item()
{

}

Item::~Item()
{

}

double Item::totalPrice() const //returns _count*_unitPrice
{
	return _count * _unitPrice;
}

bool Item::operator <(const Item& other) const //compares the _serialNumber of those items.
{
	return (_serialNumber < other._serialNumber);
}

bool Item::operator >(const Item& other) const //compares the _serialNumber of those items.
{
	return (!(*this < other));
}

bool Item::operator ==(const Item& other) const //compares the _serialNumber of those items.
{
	return (this->_serialNumber == other._serialNumber);
}

void Item::inc_count()
{
	_count++;
}
void Item::dec_count()
{
	_count--;
}
// ----------------- getters-----------
double Item::get_price()
{
	return _unitPrice;
}

string Item::get_name()
{
	return _name;
}

int Item::get_count()
{
	return _count;
}