#pragma once
#include"Item.h"
#include<set>

class Customer
{
public:
	Customer(std::string name);
	Customer();
	~Customer();
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void deleteItem(Item);//remove item from the set

	bool item_exists(Item other);
	void inc_item(Item item);
	void remove_item(Item item);

	//get and set functions
	void print_items();
	string get_name();

private:
	string _name;
	set<Item> _items;

};
